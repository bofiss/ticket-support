'use strict'

const User = use('App/Model/User')
const Validator = use('Validator')



class AuthController {
    /**
     * Show register page
     * @param {*} request 
     * @param {*} response 
     */
    * showRegisterPage(request, response) {
        yield response.sendView('auth.register')
    }
    
    /**
     * Handle user registration
     * @param {*} request 
     * @param {*} response 
     */
    * register(request, response) {
        const validation = yield Validator.validateAll(request.all(), User.rules)

        // show errors message upon validation fail
        if(validation.fails()) {
            yield request
                .withAll()
                .andWith({errors: validation.messages() })
                .flash()
           return response.redirect('back')
        }

        // persist to database
        const user = yield User.create({
            username: request.input('username'),
            email: request.input('email'),
            password: request.input('password')
        })

       // login the user
       yield request.auth.login(user)

       //redirect to hommepage
       response.redirect('/')

    }


    /**
     * Show login page
     * @param {*} request 
     * @param {*} response 
     */
    * showLoginPage(request, response) {
        yield response.sendView('auth.login')
    }


    * login(request, response) {
        const email = request.input('email')
        const password = request.input('password')

        try {
            yield request.auth.attempt(email, password)

            // redirect to homepage
            response.redirect('/')
        } catch (e) {
            yield request.with({error: 'Invalid credentiels'}).flash()

            // redirect back with error
            response.redirect('back')
        }
    }

    * logout(request, response) {
        yield request.auth.logout()

        // redirect to login page
        response.redirect('/login')
    }
  
}

module.exports = AuthController
